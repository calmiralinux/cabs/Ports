#!/usr/bin/python3

import os
import toml
import wget
import hashlib

import libcport.datatypes as cd

CACHE = "./.cache"

if not os.path.isdir(CACHE):
    os.makedirs(CACHE)

METADATA = toml.load("/usr/ports/metadata.toml")
PORT_LIST = METADATA['port_sys']['ports']

for port in PORT_LIST:
    port = cd.port(port)
    port_config = toml.load(f"../ports/{port.name}/port.toml")
    port_download_url = port_config['port'].get('url')
    
    if port_download_url is None or port_download_url == "none":
        continue
    
    wget.download(port_download_url, CACHE)
    fname = f"{CACHE}/{wget.detect_filename(port_download_url)}"
    _hash = hashlib.sha256()

    with open(fname, 'rb') as f:
        for blk in iter(lambda: f.read(4096), b""):
            _hash.update(blk)
    
    sha256 = _hash.hexdigest()
    
    port_config['port']['sha256'] = sha256
    
    with open(f"../ports/{port.name}/port.toml", 'w') as f:
        toml.dump(port_config, f)

shutil.rmtree(CACHE)
