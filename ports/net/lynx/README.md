# lynx-2.8.9rel.1

Text based WEB browser

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 31.0 Mb

### Dependencies

- **required:** none
- **recommend:** none
- **optional:** postcpl/gnutls general/zip general/unzip general/sharutils
- **conflict:** none

