# curl-7.81.0

Utility and a library used for transferring files with URL syntax to any of the following protocols: FTP FTPS HTTP HTTPS SCP SFTP TFTP TELNET DICT LDAP LDAPS and FILE. Its ability to both download and upload files can be incorporated into other programs to support functions like streaming media

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 2.4 Mb

### Dependencies

- **required:** none
- **recommend:**  base/make-ca
- **optional:** none
- **conflict:** none

