# btrfs-progs-5.16.2

The administration and debugging tools for the BTRFS

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov <linuxoid85@gmail.com>
- **releases:**  'v1.2a2' 'v1.2a3'
- **usage:** 51 Mb

### Dependencies

- **required:**  general/lzo
- **recommend:**  general/python_modules/asciidoc pst/xmlto
- **optional:**  postcpl/lvm2 postcpl/reiserfsprogs
- **conflict:** none

