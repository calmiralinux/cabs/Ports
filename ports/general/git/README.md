# git-2.37.3

Git is a free and open source distributed version control system designed to handle everything from small to very large projects with speed and efficiency

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 231.0 Mb

### Dependencies

- **required:**  base/curl
- **recommend:** none
- **optional:** none
- **conflict:** none

