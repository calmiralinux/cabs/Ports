# gobject-introspection-1.72.0

GObject Introspection is used to describe the program APIs and collect them in a uniform machine readable format

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 42.0 Mb

### Dependencies

- **required:**  general/glib
- **recommend:** none
- **optional:** xorg/x11-libs/cairo xorg/gnome/gjs general/gtk-doc general/pymo/mako
- **conflict:** none

