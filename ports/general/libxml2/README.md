# libxml2-2.9.13

Libraries and utilities used for parsing XML files

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v1.2a2' 'v1.2a3'
- **usage:** 103 Mb

### Dependencies

- **required:**  
- **recommend:**  general/icu
- **optional:**  
- **conflict:**  

