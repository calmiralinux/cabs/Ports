# gtk-doc-1.33.2

The GTK-Doc package contains a code documenter

## Detailed information
### Port/package

- **maintainer:** Sergey Gaberer <nordic.dev@pm.me>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 12 Mb

### Dependencies

- **required:** pst/docbook-xml pst/docbook-xsl pst/itstool general/libxlst
- **recommend:** none
- **optional:** none
- **conflict:** none

