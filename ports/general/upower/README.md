# upower-1.90.0

The UPower package provides an interface to enumerating power devices listening to device events and querying history and statistics. Any application or service on the system can access the org.freedesktop.UPower service via the system message bus

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 5.1 Mb

### Dependencies

- **required:** general/libgudev general/libusb postcpl/polkit
- **recommend:** none
- **optional:** general/gobject-introspection general/gtk-doc
- **conflict:** none

