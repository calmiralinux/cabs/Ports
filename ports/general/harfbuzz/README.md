# harfbuzz-5.2.0

The HarfBuzz package contains an OpenType text shaping engine

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 135.0 Mb

### Dependencies

- **required:** none
- **recommend:** general/gobject-introspection general/glib general/icu general/freetype
- **optional:** xorg/x11-libs/cairo general/git general/gtk-doc
- **conflict:** none

