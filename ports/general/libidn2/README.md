# libidn2-2.3.2

libidn2 is a package designed for internationalized string handling based on standards from the Internet Engineering Task Force (IETF)'s IDN working group designed for internationalized domain names

## Detailed information
### Port/package

- **maintainer:** Sergey Gaberer <nordic.dev@pm.me>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 25 Mb

### Dependencies

- **required:** general/libunistring
- **recommend:** none
- **optional:** general/git general/gtk-doc
- **conflict:** none

