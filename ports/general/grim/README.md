# grim-1.4.0

Grab images from a Wayland compositor

## Detailed information
### Port/package

- **maintainer:** Sergey Gaberer <nordic.dev@pm.me>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 1 Mb

### Dependencies

- **required:** base/meson general/pixman general/libpng wayland/wayland
- **recommend:** none
- **optional:** general/libjpeg
- **conflict:** none

