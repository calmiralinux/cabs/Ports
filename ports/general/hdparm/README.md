# hdparm-9.65

Utility that is useful for obtaining information about and controlling ATA/IDE controllers and hard drives. It allows to increase performance and sometimes to increase stability

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 1.0 Mb

### Dependencies

- **required:** none
- **recommend:** none
- **optional:** none
- **conflict:** none

