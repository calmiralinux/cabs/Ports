# doxygen-1.9.3

A documentation system for C++ C Java Objective-C Corba IDL and to some extent PHP C# and D. It is useful for generating HTML documentation and/or an off-line reference manual from a set of documented source files. There is also support for generating output in RTF PostScript hyperlinked PDF compressed HTML and Unix man pages. The documentation is extracted directly from the sources which makes it much easier to keep the documentation consistent with the source code

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 170 Mb

### Dependencies

- **required:**  base/cmake general/git
- **recommend:** none
- **optional:**  general/libxml2 general/llvm pst/texlive
- **conflict:** none

