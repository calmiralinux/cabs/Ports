# wlr-randr-0.2.0

Utility to manage outputs of a Wayland compositor

## Detailed information
### Port/package

- **maintainer:** Sergey Gaberer <nordic.dev@pm.me>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 1 Mb

### Dependencies

- **required:**   base/meson wayland/wayland 
- **recommend:** none
- **optional:** none
- **conflict:** none

