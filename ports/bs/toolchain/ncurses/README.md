# ncurses-6.3

Libraries for terminal-independent handling of character screens

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 50.0 Mb

### Dependencies

- **required:** none
- **recommend:** none
- **optional:** none
- **conflict:** none

