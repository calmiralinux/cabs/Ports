# linux-api-headers-5.19.8

The kernel's API for use by GNU C Library

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 1433.6 Mb

### Dependencies

- **required:** none
- **recommend:** none
- **optional:** none
- **conflict:** none

