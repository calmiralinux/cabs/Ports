# libdrm-2.4.113

libdrm provides a user space library for accessing the DRM direct rendering manager on operating systems that support the ioctl interface. libdrm is a low-level library typically used by graphics drivers such as the Mesa DRI drivers the X drivers libva and similar projects

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 9.0 Mb

### Dependencies

- **required:** none
- **recommend:**  xorg/x11-libs/xlibs
- **optional:**  base/cmake
- **conflict:** none

