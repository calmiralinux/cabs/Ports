# fluxbox-1.3.7

The Fluxbox package contains a window manager

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 101 Mb

### Dependencies

- **required:** none
- **recommend:** none
- **optional:** general/dbus general/fribidi xorg/x11-libs/imlib
- **conflict:** none

