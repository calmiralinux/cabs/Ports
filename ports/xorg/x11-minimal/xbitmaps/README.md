# xbitmaps-1.1.2

Bitmap images used by multiple applications built in Xorg

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 1.6 Mb

### Dependencies

- **required:**  xorg/x11-minimal/util-macros
- **recommend:** none
- **optional:** none
- **conflict:** none

