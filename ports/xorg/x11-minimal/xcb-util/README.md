# xcb-util-0.4.0

The xcb-util package provides additional extensions to the XCB library many that were previously found in Xlib but are not part of core X protocol

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 2.6 Mb

### Dependencies

- **required:**  xorg/x11-libs/libxcb
- **recommend:** none
- **optional:**  general/doxygen
- **conflict:** none

