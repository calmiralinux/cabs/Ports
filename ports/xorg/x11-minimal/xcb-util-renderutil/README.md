# xcb-util-renderutil-0.3.9

Additional extensions to the XCB library

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 2.4 Mb

### Dependencies

- **required:**  xorg/x11-libs/libxcb
- **recommend:** none
- **optional:**  general/goxygen
- **conflict:** none

