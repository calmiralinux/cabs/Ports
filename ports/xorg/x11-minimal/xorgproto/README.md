# xorgproto-2021.5

The header files required to build the X

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov <linuxoid85@gmail.com>
- **releases:**  'v1.2a2' 'v1.2a3'
- **usage:** 8.3 Mb

### Dependencies

- **required:**  xorg/x11-minimal/util-macros
- **recommend:**  
- **optional:** pst/fop pst/xmlto
- **conflict:**  

