# xlegacy-0.1

Xorg's ancestor (X11R1 in 1987) at first only provided bitmap fonts with a tool (bdftopcf) to assist in their installation. With the introduction of xorg-server-1.19.0 and libXfont2 many people will not need them. There are still a few old packages which might require or benefit from these deprecated fonts and so the following packages

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 30 Mb

### Dependencies

- **required:**  xorg/x11-minimal/xcursor-themes
- **recommend:** none
- **optional:** none
- **conflict:** none

