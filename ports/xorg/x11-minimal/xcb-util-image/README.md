# xcb-util-image-0.4.0

The xcb-util-image package provides additional extensions to the XCB library

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 3.2 Mb

### Dependencies

- **required:**  xorg/x11-minimal/xcb-util
- **recommend:** none
- **optional:**  general/doxygen
- **conflict:** none

