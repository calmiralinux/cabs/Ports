# util-macros-1.19.3

The m4 macros used by all of the Xorg packages.

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 0.632 Mb

### Dependencies

- **required:**  xorg/x11-minimal/XBE
- **recommend:** none
- **optional:** none
- **conflict:** none

