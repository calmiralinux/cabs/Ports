# sed-4.8

The Sed package contains a stream editor

## Detailed information
### Port/package

- **maintainer:** Sergey Gaberer <nordic.dev@pm.me>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 22 Mb

### Dependencies

- **required:** base/bash base/binutils base/coreutils base/gcc base/glibc base/grep base/gettext base/make base/sed base/texinfo base/acl base/attr base/e2fsprogs base/file base/libtool base/shadow
- **recommend:** none
- **optional:** none
- **conflict:** none

