# zlib-1.2.12

Compression and decompression routines used by some programs

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 5.0 Mb

### Dependencies

- **required:** base/bash base/binutils base/coreutils base/gcc base/glibc base/make base/sed
- **recommend:** none
- **optional:** none
- **conflict:** none

