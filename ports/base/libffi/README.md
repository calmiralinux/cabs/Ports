# libffi-3.4.2

The Libffi library provides a portable high level programming interface to various calling conventions.

## Detailed information
### Port/package

- **maintainer:** Sergey Gaberer <nordic.dev@pm.me>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 9 Mb

### Dependencies

- **required:** base/bash base/binutils base/coreutils base/gcc base/glibc base/make base/sed base/zlib base/python
- **recommend:** none
- **optional:** none
- **conflict:** none

