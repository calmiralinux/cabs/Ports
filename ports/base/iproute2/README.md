# iproute2-5.18.0

The IPRoute2 package contains programs for basic and advanced IPV4-based networking.

## Detailed information
### Port/package

- **maintainer:** Sergey Gaberer <nordic.dev@pm.me>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 24 Mb

### Dependencies

- **required:** base/bash base/bison base/coreutils base/flex base/gcc base/glibc base/make base/libcap base/libelf base/sed base/zlib toolchain/linux-api-headers
- **recommend:** none
- **optional:**   base/iptables 
- **conflict:** none

