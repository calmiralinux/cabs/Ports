# flex-2.6.4

The Flex package contains a utility for generating programs that recognize patterns in text.

## Detailed information
### Port/package

- **maintainer:** Sergey Gaberer <nordic.dev@pm.me>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 16 Mb

### Dependencies

- **required:** base/bash base/binutils base/coreutils base/gcc base/glibc base/grep base/ncurses base/make base/gettext base/m4 base/patch base/sed base/texinfo base/iproute2 base/kbd base/kmod base/man-db
- **recommend:** none
- **optional:** none
- **conflict:** none

