# sys_configs-0.1

Базовые конфиги системы

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov <linuxoid85@gmail.com>
- **releases:**  'v1.2a2' 'v1.2a3'
- **usage:** 0.1 Mb

### Dependencies

- **required:**  base/coreutils
- **recommend:** none
- **optional:** none
- **conflict:** none

