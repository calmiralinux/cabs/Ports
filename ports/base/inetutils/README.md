# inetutils-2.3

The Inetutils package contains programs for basic networking.

## Detailed information
### Port/package

- **maintainer:** Sergey Gaberer <nordic.dev@pm.me>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 32 Mb

### Dependencies

- **required:** base/bash base/binutils base/coreutils base/gcc base/glibc base/grep base/ncurses base/make base/patch base/readline base/sed base/texinfo base/zlib base/tar
- **recommend:** none
- **optional:** none
- **conflict:** none

