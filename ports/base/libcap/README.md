# libcap-2.65

The Libcap package implements the user-space interfaces to the POSIX 1003.1e capabilities available in Linux kernels.

## Detailed information
### Port/package

- **maintainer:** Sergey Gaberer <nordic.dev@pm.me>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 18 Mb

### Dependencies

- **required:** base/bash base/binutils base/attr base/coreutils base/gcc base/glibc base/make base/perl base/sed base/iproute2 base/shadow
- **recommend:** none
- **optional:**   postcpl/linux-pam 
- **conflict:** none

