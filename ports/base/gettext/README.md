# gettext-0.21

The Gettext package contains utilities for internationalization and localization.

## Detailed information
### Port/package

- **maintainer:** Sergey Gaberer <nordic.dev@pm.me>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 224 Mb

### Dependencies

- **required:** base/bash base/binutils base/coreutils base/gawk base/gcc base/glibc base/grep base/ncurses base/make base/sed base/texinfo base/bison base/automake
- **recommend:** none
- **optional:** none
- **conflict:** none

