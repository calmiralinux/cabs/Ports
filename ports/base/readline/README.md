# readline-8.1.2

Set of libraries that offers command-line editing and history capabilities

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 15 Mb

### Dependencies

- **required:** base/bash base/binutils base/coreutils base/gawk base/gcc base/glibc base/grep base/make base/ncurses base/patch base/sed base/texinfo
- **recommend:** none
- **optional:** none
- **conflict:** none

