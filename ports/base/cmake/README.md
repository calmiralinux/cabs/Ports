# cmake-3.23.2

The CMake package contains a modern toolset used for generating Makefiles

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 415 Mb

### Dependencies

- **required:**  base/libuv
- **recommend:**  basicnet/curl general/libarchive basicnet/nghttp2
- **optional:**  base/gcc general/git general/mercurial xorg/x11-libs/qt
- **conflict:** none

