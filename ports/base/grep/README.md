# grep-3.7

The Grep package contains programs for searching through the contents of files.

## Detailed information
### Port/package

- **maintainer:** Sergey Gaberer <nordic.dev@pm.me>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 26 Mb

### Dependencies

- **required:** base/bash base/binutils base/coreutils base/diffutils base/gettext base/gcc base/glibc base/grep base/make base/patch base/sed base/texinfo
- **recommend:** none
- **optional:** base/pcre base/libsigsegv
- **conflict:** none

