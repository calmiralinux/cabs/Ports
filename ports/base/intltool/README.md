# intltool-0.51

The Intltool is an internationalization tool used for extracting translatable strings from source files.

## Detailed information
### Port/package

- **maintainer:** Sergey Gaberer <nordic.dev@pm.me>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 2 Mb

### Dependencies

- **required:** base/bash base/gawk base/glibc base/make base/perl base/sed base/xml::parser base/autoconf base/automake base/grep
- **recommend:** none
- **optional:** none
- **conflict:** none

