# gcc-12.1.0

GNU compiler collection which includes the C and C++ compilers

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 4710.4 Mb

### Dependencies

- **required:** base/bash base/binutils base/diffutils base/findutils base/gawk base/gcc base/gettext base/glibc base/gmp base/grep base/m4 base/make base/mpc base/mpfr base/patch base/perl base/python base/sed base/tar base/texinfo base/zstd
- **recommend:**  base/shadow
- **optional:** none
- **conflict:** none

