# check-0.15.2

Check is a unit testing framework for C.

## Detailed information
### Port/package

- **maintainer:** Sergey Gaberer <nordic.dev@pm.me>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 11 Mb

### Dependencies

- **required:** base/bash base/gawk base/gcc base/grep base/ncurses base/make base/sed base/texinfo
- **recommend:** none
- **optional:** none
- **conflict:** none

