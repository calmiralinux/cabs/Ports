# nvim-0.7.2

Hyperextensible Vim-based text editor

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 217.0 Mb

### Dependencies

- **required:** base/gettext base/make base/cmake base/glibc base/lua base/gcc
- **recommend:**  general/luajit
- **optional:** none
- **conflict:** none

