# coreutils-9.1

The Coreutils package contains utilities for showing and setting the basic system characteristics

## Detailed information
### Port/package

- **maintainer:** Sergey Gaberer <nordic.dev@pm.me>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 166 Mb

### Dependencies

- **required:** base/bash base/binutils base/coreutils base/gcc base/glibc base/gmp base/grep base/libcap base/make base/openssl base/patch base/perl base/sed base/texinfo base/eudev base/findutils base/shadow base/man-db
- **recommend:** none
- **optional:**   postcpl/expect postcpl/io::tty 
- **conflict:** none

