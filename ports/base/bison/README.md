# bison-3.8.2

Bison is a general-purpose parser generator that converts an annotated context-free grammar into a deterministic LR or generalized LR (GLR) parser employing LALR(1) parser tables.

## Detailed information
### Port/package

- **maintainer:** Sergey Gaberer <nordic.dev@pm.me>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 60 Mb

### Dependencies

- **required:** base/bash base/binutils base/coreutils base/gcc base/gettext base/glibc base/make base/grep base/m4 base/perl base/sed
- **recommend:** none
- **optional:**  general/doxygen
- **conflict:** none

