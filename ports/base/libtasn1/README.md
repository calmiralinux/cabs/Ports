# libtasn1-4.18.0

ibtasn1 is a highly portable C library that encodes and decodes DER/BER data following an ASN.1 schema

## Detailed information
### Port/package

- **maintainer:** Sergey Gaberer <nordic.dev@pm.me>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 15 Mb

### Dependencies

- **required:** base/bash base/binutils base/coreutils base/gcc base/glibc base/make
- **recommend:** none
- **optional:**   general/valgrind 
- **conflict:** none

